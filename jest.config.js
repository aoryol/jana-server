module.exports = {
  moduleNameMapper: {
    "^~/(.*)$": "<rootDir>/src/$1"
  },
  transform: {
    "^.+\\.ts$": "ts-jest"
  },
  collectCoverageFrom: [
    "src/**/*.ts"
  ],
  moduleFileExtensions: ["js", "ts", "json", "node"],
  testRegex: "/test/.*\\.test\\.ts$",
  globalSetup: "<rootDir>/test/test-utils/setup.js",
  globalTeardown: "<rootDir>/test/test-utils/teardown.js"
};
