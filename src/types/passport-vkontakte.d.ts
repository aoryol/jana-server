declare module "passport-vkontakte" {
  export class Strategy {
    constructor(options: any, verify: any);

    public authenticate(req: any, options: any): any;
    public authorizationParams(options: any): any;
    public parseErrorResponse(body: any, status: any): any;
    public userProfile(accessToken: any, done: any): any;
  }

  export const version: string;

  export namespace Strategy {
    namespace prototype {
      function authenticate(req: any, options: any): any;

      function authorizationParams(options: any): any;

      function parseErrorResponse(body: any, status: any): any;

      function tokenParams(options: any): any;

      function userProfile(accessToken: any, done: any): any;

      namespace authenticate {
        const prototype: {};
      }

      namespace authorizationParams {
        const prototype: {};
      }

      namespace parseErrorResponse {
        const prototype: {};
      }

      namespace tokenParams {
        const prototype: {};
      }

      namespace userProfile {
        const prototype: {};
      }
    }
  }
}
