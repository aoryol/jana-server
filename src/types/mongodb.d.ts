import * as mongodb from "mongodb";

type Omit<T, K> = Pick<T, Exclude<keyof  T, K>>;

declare module "mongodb" {
  interface UpdateOptions {
    arrayFilters: object[];
  }

  interface Collection<TSchema = Default> {
    updateMany(
      filter: FilterQuery<TSchema>,
      update: object, options: CommonOptions & UpdateOptions & { upsert?: boolean },
      callback: MongoCallback<UpdateWriteOpResult>): void;

    updateOne(
      filter: FilterQuery<TSchema>,
      update: object,
      options?: ReplaceOneOptions & UpdateOptions
    ): Promise<UpdateWriteOpResult>;

    findOneAndUpdate(
      filter: FilterQuery<TSchema>,
      update: object,
      options?: FindOneAndReplaceOption & UpdateOptions
    ): Promise<FindAndModifyWriteOpResultObject<TSchema>>;

    insertOne(
      docs: Omit<TSchema, "_id">,
      options?: CollectionInsertOneOptions
    ): Promise<InsertOneWriteOpResult>;
  }
}
