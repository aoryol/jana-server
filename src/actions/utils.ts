export function parseBoolean(param?: string) {
  if (!param) {
    return false;
  }

  try {
    return !!JSON.parse(param.toLowerCase());
  } catch (e) {
    return !!param;
  }
}
