import Bluebird from "bluebird";
import _ from "lodash";
import getSlug from "speakingurl";

import { EntityId, Note } from "~/models/db";
import { Note as ClientNote, WebContext } from "~/models/web";

export async function getList(ctx: WebContext) {
  function buildCriteria() {
    const { timestamp } = ctx.request.query;

    const userId = ctx.db.objectId(ctx.user.id);
    const criteria: any = { userId };

    if (timestamp) {
      criteria.dateModified = { $gt: new Date(+timestamp) };
    } else {
      criteria.isDeleted = { $ne: true };
    }

    return criteria;
  }

  const notes = await ctx.db.notes
    .find<Note>(buildCriteria())
    .project({ userId: 0 })
    .sort({ dateModified: 1 })
    .toArray();

  ctx.body = notes.map(mapNote.bind(null, ctx));
}

export async function getById(ctx: WebContext) {
  const id = ctx.db.objectId(ctx.params.id);
  const userId = ctx.db.objectId(ctx.user.id);

  const note = await ctx.db.notes.findOne({ _id: id, userId });

  if (!note) {
    return ctx.throw("Note not found", 404);
  }

  ctx.body = mapNote(ctx, note);
}

export async function getByPublicName(ctx: WebContext) {
  const publicName = ctx.params.publicName;

  const note = await ctx.db.notes.findOne({ publicName });

  if (!note) {
    return ctx.throw("Note not found", 404);
  }

  ctx.body = mapNote(ctx, note);
}

export async function modify(ctx: WebContext) {
  function getTimestamp() {
    const { timestamp: reqTimestamp } = ctx.request.query;
    return new Date(reqTimestamp ? +reqTimestamp : 0);
  }

  function getNoteModificationProps(note: ClientNote) {
    const now = new Date();

    const basicProps = {
      ..._.pick(note, "title", "body"),
      ...basicModificationProps(ctx),
      isDeleted: note.action === "remove"
    };

    const insertProps = {
      ...basicProps,
      userId,
      dateCreated: now
    };

    const refreshDateProps = {
      dateModified: now
    };

    return { basicProps, insertProps, refreshDateProps };
  }

  async function updateExistingNote(noteId: EntityId, basicProps: any, refreshDateProps: any): Promise<boolean> {
    const updateRes = await ctx.db.notes.findOneAndUpdate(
      { _id: noteId, dateModified: { $lte: timestamp } },
      { $set: basicProps }
    );

    if (!updateRes.value) {
      return false;
    }

    if (basicProps.isDeleted && !_.isEmpty(updateRes.value.versions)) {
      // remove this note ref from other versions
      await ctx.db.notes.updateMany(
        { _id: { $in: updateRes.value.versions } },
        {
          $pull: { versions: noteId },
          $set: refreshDateProps
        }
      );
    }

    return true;
  }

  async function modifyNote(note: ClientNote): Promise<void> {
    const { basicProps, insertProps, refreshDateProps } = getNoteModificationProps(note);

    if (note.action === "add") {
      await ctx.db.notes.insertOne(insertProps);
      return;
    }

    const noteId = ctx.db.objectId(note.id);
    const currentNote = await ctx.db.notes.findOne(
      { _id: noteId, userId },
      { projection: { dateModified: 1 } }
    );

    // note doesn't exist or it's another user note
    if (!currentNote) {
      return;
    }

    // try to update existing note if there is no conflicts
    if (currentNote.dateModified <= timestamp && await updateExistingNote(noteId, basicProps, refreshDateProps)) {
      return;
    }

    // just update modification timestamp to receive updates for note
    // if it was updated from another client and deleted from the current one
    if (basicProps.isDeleted) {
      await ctx.db.notes.updateOne(
        { _id: noteId },
        { $set: refreshDateProps }
      );

      return;
    }

    // branch note into 2 versions
    const insertRes = await ctx.db.notes.insertOne({ ...insertProps, versions: [noteId] });
    const newNoteId = insertRes.insertedId;

    await ctx.db.notes.updateOne(
      { _id: noteId },
      {
        $push: { versions: newNoteId },
        $set: refreshDateProps
      }
    );
  }

  const userId = ctx.db.objectId(ctx.user.id);
  const timestamp = getTimestamp();

  const clientNotes: ClientNote[] = _(ctx.request.body)
    .orderBy(["timestamp"], ["asc"])
    .keyBy("id")
    .toArray()
    .value();

  await Bluebird.each(clientNotes, modifyNote);

  const serverNotes = await ctx.db.notes
    .find({ userId, dateModified: { $gt: timestamp } })
    .project({ userId: 0 })
    .sort({ dateModified: 1 })
    .toArray();

  ctx.body = serverNotes.map(mapNote.bind(null, ctx));
}

export async function publish(ctx: WebContext) {
  const id = ctx.db.objectId(ctx.params.id);
  const userId = ctx.db.objectId(ctx.user.id);

  const note = await ctx.db.notes.findOne({ _id: id, userId });
  if (!note) {
    return ctx.throw("Note not found", 404);
  }

  const titleSlug = getSlug(note.title, { truncate: 20 });
  const publicName = `${titleSlug}-${note._id.toHexString()}`;

  await ctx.db.notes.updateOne(
    { _id: id, userId },
    { $set: { publicName, ...basicModificationProps(ctx) } }
  );

  ctx.body = mapNote(ctx, { ...note, publicName });
}

export async function unpublish(ctx: WebContext) {
  const id = ctx.db.objectId(ctx.params.id);
  const userId = ctx.db.objectId(ctx.user.id);

  const note = await ctx.db.notes.findOne({ _id: id, userId });
  if (!note) {
    return ctx.throw("Note not found", 404);
  }

  await ctx.db.notes.updateOne(
    { _id: id, userId },
    {
      $unset: { publicName: null },
      $set: basicModificationProps(ctx)
    }
  );

  ctx.body = mapNote(ctx, _.omit(note, "publicName"));
}

const basicModificationProps = (ctx: WebContext) => ({
  dateModified: new Date(),
  modifiedBy: ctx.user.client!
});

const mapNote = (ctx: WebContext, note: Note): any => ({
  ...ctx.db.stringifyEntityId(_.omit(note, "userId")),
  isDeleted: !!note.isDeleted,
  versions: note.versions || []
});
