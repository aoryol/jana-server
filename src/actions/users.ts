import { database } from "~/config/db";

import { UserClient } from "~/models/db";
import { SessionUser, WebContext } from "~/models/web";

export function getCurrent(ctx: WebContext) {
  ctx.body = ctx.user;
}

export async function findOrCreate(
  provider: string,
  profile: { externalId: string; displayName: string; }
): Promise<SessionUser> {
  const dbUser = await database.users
    .findOne({ identities: { provider, externalId: profile.externalId } });

  if (dbUser) {
    return {
      id: dbUser._id.toHexString(),
      displayName: dbUser.displayName
    };
  }

  const insertResult = await database.users.insertOne({
    displayName: profile.displayName,
    identities: [{ provider, externalId: profile.externalId }],
    clients: []
  });

  return {
    id: insertResult.insertedId.toHexString(),
    displayName: profile.displayName
  };
}

export async function updateClients(sessionUser: SessionUser, client: UserClient): Promise<void> {
  const updResult = await database.users.updateOne(
    { _id: database.objectId(sessionUser.id), "clients.id": client.id },
    { $set: { "clients.$": client } }
  );

  if (updResult.modifiedCount === 0) {
    await database.users.updateOne(
      { _id: database.objectId(sessionUser.id), "clients.id": { $ne: client.id } },
      { $push: { clients: client } }
    );
  }
}
