import * as _ from "lodash";
import mongo from "mongodb";

import * as models from "~/models/db";
import settings from "./settings";

let client: mongo.MongoClient | null;
export let database: models.Database;

export async function connect(): Promise<models.Database> {
  if (client) {
    throw new Error("Connection already created");
  }

  client = await mongo.MongoClient.connect(settings.data.mongo.url, { useNewUrlParser: true });
  database = await initSchema(client.db(settings.data.mongo.dbName));

  return database;
}

export async function disconnect({ force = settings.data.mongo.forceConnectionClose }: { force?: boolean } = {}) {
  if (client) {
    await client.close(force);
    client = null;
  }
}

async function initSchema(db: mongo.Db): Promise<models.Database> {
  const users = db.collection("users");
  await users.createIndex("identities");

  const notes = db.collection("notes");
  await notes.createIndex("userId");
  await notes.createIndex("publicName", {
    unique: true,
    partialFilterExpression: {
      publicName: { $type: "string" }
    }
  });

  return Object.assign(db, {
    users, notes,

    objectId(s: string) {
      return new mongo.ObjectID(s);
    },

    stringifyEntityId(entity: any) {
      return {
        id: entity._id.toHexString(),
        ..._.omit(entity, "_id")
      };
    },

    parseEntityId(entity: any) {
      return {
        _id: new mongo.ObjectID(entity.id),
        ..._.omit(entity, "id")
      };
    }
  });
}
