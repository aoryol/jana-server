import koa from "koa";

import { database } from "./db";

export function initContext(ctx: koa.BaseContext) {
  Object.defineProperty(ctx, "db", { get() { return database; } });
  Object.defineProperty(ctx, "user", { get() { return (this as any).state.user; } });
}
