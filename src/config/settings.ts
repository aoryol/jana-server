type Env = "development" | "test" | "production";
const env = (process.env.NODE_ENV || "development").toLowerCase() as Env;

function parseOptionalInt(str: string | undefined, defaultValue: number): number {
  return str ? parseInt(str, 10) : defaultValue;
}

function parseBoolean(str: string): boolean {
  return str.toLowerCase() === "true" || str === "1";
}

function envValue(key: string, defaultValue?: string, testValue?: string): string {
  const val = process.env[key];
  if (val) {
    return val;
  }

  if (testValue !== undefined && env === "test") {
    return testValue;
  }

  return defaultValue || "";
}

const webPort = parseOptionalInt(process.env.PORT, 3000);
const webUrl = envValue("JANA_WEB_URL", `http://localhost:${webPort}`);

export default {
  env,
  auth: {
    signingKey: envValue("JANA_AUTH_SIGNING_KEY", "JANA secret key"),
    allowedRedirectUrls: envValue("JANA_AUTH_REDIRECT_URLS").split("|")
  },
  api: {
    github: {
      clientId: envValue("JANA_API_GITHUB_CLIENT_ID", undefined, "__test"),
      secret: envValue("JANA_API_GITHUB_CLIENT_SECRET", undefined, "__test")
    },
    vk: {
      clientId: envValue("JANA_API_VK_CLIENT_ID", undefined, "__test"),
      secret: envValue("JANA_API_VK_CLIENT_SECRET", undefined, "__test")
    }
  },
  logger: {
    dev: env === "development" || !!process.env.JANA_LOGGER_DEV
  },
  data: {
    mongo: {
      url: envValue("JANA_MONGO_URL", "mongodb://localhost:27017"),
      dbName: envValue("JANA_MONGO_DB_NAME", "jana", "jana-test"),
      forceConnectionClose: parseBoolean(envValue("JANA_MONGO_FORCE_CLOSE", "false", "true"))
    }
  },
  server: {
    web: {
      port: webPort,
      url: webUrl,
      corsOrigins: envValue("JANA_WEB_CORS_ORIGINS", "*").split("|")
    }
  }
};
