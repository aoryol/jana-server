import _ from "lodash";

import Router from "koa-router";

import jwt from "jsonwebtoken";
import passport from "koa-passport";
import passportTypes from "passport";
import passportGithub from "passport-github";
import passportJwt from "passport-jwt";
import passportVk from "passport-vkontakte";

import { DEFAULT_USER_CLIENT, UserClient } from "~/models/db";
import { SessionUser, WebContext } from "~/models/web";

import * as userActions from "~/actions/users";

import settings from "./settings";

declare module "passport" {
  interface AuthenticateOptions {
    callbackURL?: string;
  }
}

function useOAuth2Strategy(provider: string, strategy: any) {
  const apiOptions: any = (settings.api as any)[provider];

  passport.use(provider, new strategy({
      clientID: apiOptions.clientId,
      clientSecret: apiOptions.secret,
      callbackURL: `/auth/${provider}/callback`
    }, async (
      accessToken: string,
      refreshToken: string,
      profile: passportTypes.Profile,
      done: (error: any, user?: any) => void
    ) => {
      const user = await userActions.findOrCreate(provider, {
        externalId: profile.id,
        displayName: profile.displayName
      });

      done(null, user);
    }
  ));
}

useOAuth2Strategy("github", passportGithub.Strategy);
useOAuth2Strategy("vk", passportVk.Strategy);

passport.use("token", new passportJwt.Strategy({
    secretOrKey: settings.auth.signingKey,
    jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken()
  }, (jwtPayload, done) => {
  done(null, _.omit(jwtPayload, ["iat", "exp"]));
}));

const makeToken = (
  user: SessionUser,
  expiresIn: string = "2h",
  client: UserClient = DEFAULT_USER_CLIENT
) => jwt.sign(
  { ...user, client },
  settings.auth.signingKey,
  {expiresIn}
);

const router = new Router();

for (const provider of ["github", "vk"]) {
  function authenticate(opts = {}) {
    return (ctx: Router.IRouterContext, next: () => Promise<any>) => {
      const {redirectUrl} = ctx.query;
      if (!settings.auth.allowedRedirectUrls.includes(redirectUrl)) {
        ctx.throw(403);
      }

      const redirectParam = `redirectUrl=${encodeURIComponent(redirectUrl)}`;

      const authOptions = {
        ...opts,
        callbackURL: `${settings.server.web.url}/auth/${provider}/callback?${redirectParam}`
      };

      return passport.authenticate(provider, authOptions)(ctx, next);
    };
  }

  router.get(`/${provider}`, authenticate());

  router.get(`/${provider}/callback`,
    authenticate({ session: false }),
    (ctx: WebContext) => {
      const token = makeToken(ctx.user, "10m");
      const redirectUrl = `${ctx.query.redirectUrl}#token=${token}`;
      ctx.redirect(redirectUrl);
    }
  );
}

router.post("/refresh",
  passport.authenticate("token", { session: false }),
  async (ctx: WebContext) => {
    const {
      expiresIn = "2h",
      client = DEFAULT_USER_CLIENT
    } = ctx.request.body;

    await userActions.updateClients(ctx.user, client);

    const token = makeToken(ctx.user, expiresIn, client);
    ctx.body = { token };
  }
);

export default { passport, router, makeToken };
