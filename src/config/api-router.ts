import KoaRouter from "koa-router";

import * as notes from "~/actions/notes";
import * as users from "~/actions/users";

const privateApi = new KoaRouter()
  .get("/users/me", users.getCurrent)

  .get("/notes", notes.getList)
  .get("/notes/:id", notes.getById)

  .patch("/notes", notes.modify)

  .post("/notes/:id/public-name", notes.publish)
  .delete("/notes/:id/public-name", notes.unpublish)
;

const publicApi = new KoaRouter()
  .get("/notes/:publicName", notes.getByPublicName)
;

export default { privateApi, publicApi };
