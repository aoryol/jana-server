import cors from "@koa/cors";
import Koa from "koa";
import bodyParser from "koa-bodyparser";
import compose from "koa-compose";
import logger from "koa-logger";
import mount from "koa-mount";

import apiRouter from "./api-router";
import auth from "./auth";
import settings from "./settings";
import { initContext } from "./web-app-utils";

const app = new Koa();

if (settings.logger.dev) {
  app.use(logger());
}

const corsOrigins = settings.server.web.corsOrigins;
const corsMiddlewares = corsOrigins.map(origin => cors({ origin, credentials: true }));

app.use(compose([
  ...corsMiddlewares,
  bodyParser({
    enableTypes: ["json"]
  }),
  auth.passport.initialize()
]));

app.use(mount("/auth", compose([
  auth.router.routes(),
  auth.router.allowedMethods()
])));

app.use(mount("/api", compose([
  auth.passport.authenticate("token", { session: false }),
  apiRouter.privateApi.routes(),
  apiRouter.privateApi.allowedMethods()
])));

app.use(mount("/public-api", compose([
  apiRouter.publicApi.routes(),
  apiRouter.publicApi.allowedMethods()
])));

initContext(app.context);

export default app;
