import koa from "koa";

import { Database, UserClient } from "./db";

declare module "koa" {
  interface Context {
    readonly db: Database;
    readonly user: SessionUser;
  }
}

export type WebContext = koa.Context;

export interface SessionUser {
  id: string;
  displayName: string;
  client?: UserClient;
}

export interface Note {
  id: string;
  title: string;
  body: string;
  timestamp: number;
  action: "add" | "update" | "remove";
}
