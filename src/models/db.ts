import mongo from "mongodb";

export interface Database extends mongo.Db {
  users: mongo.Collection<User>;
  notes: mongo.Collection<Note>;

  objectId(s: string): mongo.ObjectID;
  stringifyEntityId(entity: any): any;
  parseEntityId(entity: any): any;
}

export type EntityId = mongo.ObjectID;
export type UserClientId = number;

export interface UserClient {
  id: UserClientId;
  name: string;
}

export interface User {
  _id: EntityId;
  displayName: string;
  identities: Array<{
    provider: string;
    externalId: string;
  }>;
  clients: UserClient[];
}

export interface Note {
  _id: EntityId;
  userId: EntityId;
  dateCreated: Date;
  dateModified: Date;
  modifiedBy: UserClient;
  versions?: EntityId[];
  isDeleted: boolean;
  title: string;
  body: string;
  publicName?: string;
}

export const DEFAULT_USER_CLIENT: UserClient = { id: 0, name: "<unknown>" };
