// tslint:disable-next-line:no-var-requires
require("module-alias").addAlias("~", __dirname);

import debug from "~/utils/debug";

import * as db from "~/config/db";
import settings from "~/config/settings";
import webApp from "~/config/web-app";

async function run() {
  await db.connect();

  webApp.listen(settings.server.web.port,
    () => debug(`Server started on port ${settings.server.web.port}`));
}

process.on("SIGTERM", async () => {
  await db.disconnect();
});

run();
