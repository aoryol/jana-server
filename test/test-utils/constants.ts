import { ObjectID } from "mongodb";

export const DEFAULT_USER_ID = "112233445566778899aabbcc";

export const DEFAULT_USER_OBJECT_ID = new ObjectID(DEFAULT_USER_ID);

export const OBJECT_ID_1 = new ObjectID("111111111111111111111111");
export const OBJECT_ID_2 = new ObjectID("222222222222222222222222");
export const OBJECT_ID_3 = new ObjectID("333333333333333333333333");

export const OBJECT_ID_A = new ObjectID("aaaaaaaaaaaaaaaaaaaaaaaa");
export const OBJECT_ID_B = new ObjectID("bbbbbbbbbbbbbbbbbbbbbbbb");
export const OBJECT_ID_C = new ObjectID("cccccccccccccccccccccccc");
