import Stream from "stream";
import { parse as parseUrl } from "url";

import Koa from "koa";
import KoaRouter, { IRouterContext } from "koa-router";

import { initContext } from "~/config/web-app-utils";
import { SessionUser, WebContext } from "~/models/web";

import { DEFAULT_USER_ID } from "./constants";

interface ContextMockOpts {
  url: string;
  method?: string;
  user?: boolean | SessionUser;
  params?: any;
  body?: any;
  headers?: any;
}

interface ApiRouteOpts {
  query?: string;
  params?: any;
  body?: any;
}

export async function runMiddleware(
  mw: KoaRouter.IMiddleware,
  url: string,
  ctxProps?: Partial<ContextMockOpts>
): Promise<WebContext> {
  const ctx = mockContext({ ...ctxProps, url });
  const mwResult = mw(ctx as IRouterContext, () => Promise.resolve());
  await (mwResult as Promise<void>);
  return ctx;
}

export async function runApiRoute(
  route: (ctx: WebContext) => Promise<void>,
  { query, params, body }: ApiRouteOpts = {}
): Promise<WebContext> {
  const url = "some_url" + (query ? `?${query}` : "");
  const ctx = mockContext({ url, user: true, params, body });
  await route(ctx);
  return ctx;
}

export function mockContext(
  { url, method = "GET", user, params = {}, body, headers = {} }: ContextMockOpts
): WebContext {
  const app = new Koa();
  const socket = new Stream.Duplex();

  const parsedUrl = parseUrl(url);

  const req: any = {
    ...Stream.Readable.prototype,
    socket,
    connection: socket,
    headers: {
      host: parsedUrl.host,
      ...headers
    },
    url: parsedUrl.path,
    method
  };

  const res: any = {
    ...Stream.Writable.prototype,
    socket,
    ...responseHeaders()
  };

  const ctx = app.createContext(req, res);

  ctx.params = params;
  ctx.request.body = body;

  initContext(ctx);

  if (user) {
    if (user === true) {
      user = {
        id: DEFAULT_USER_ID,
        displayName: "Default User"
      };
    }

    ctx.state.user = user;
  }

  return ctx;
}

function responseHeaders() {
  const storage: any = {};

  return {
    _headers: storage,
    getHeader(key: string) { return storage[key.toLowerCase()]; },
    setHeader(key: string, value: string) { storage[key.toLowerCase()] = value; },
    removeHeader(key: string) { delete storage[key.toLowerCase()]; }
  };
}
