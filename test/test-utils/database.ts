import * as db from "~/config/db";

export function use() {
  beforeAll(db.connect);
  afterAll(() => db.disconnect());
}

export async function fill<T = any>(collectionName: string, items: Array<Partial<T>>) {
  const collection = db.database.collection(collectionName);
  await collection.deleteMany({});
  await collection.insertMany(items);
}

export async function get<T = any>(collectionName: string, id: string) {
  const collection = db.database.collection(collectionName);
  return collection.findOne<T>({ _id: db.database.objectId(id) });
}

export async function update(collectionName: string, id: string, fields: any) {
  const collection = db.database.collection(collectionName);
  await collection.updateOne({ _id: db.database.objectId(id) }, { $set: fields });
}
