import _ from "lodash";

import settings from "~/config/settings";

export interface SettingMock {
  replace(): void;
  restore(): void;
}

export function mock(path: string, value: any): SettingMock {
  const originalValue = _.get(settings, path);

  const actions = {
    replace() { _.set(settings, path, value); },
    restore() { _.set(settings, path, originalValue); }
  };

  beforeAll(actions.replace);
  afterAll(actions.restore);

  return actions;
}
