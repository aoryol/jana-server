// tslint:disable-next-line:no-var-requires
require("module-alias").addAlias("~", `${__dirname}/../../src`);

import mongo from "mongodb";

import settings from "~/config/settings";

export async function setup() {
  const client = await mongo.MongoClient.connect(settings.data.mongo.url, { useNewUrlParser: true });
  await client.db(settings.data.mongo.dbName).dropDatabase();
  await client.close(true);
}

export async function teardown() {
  // nothing for now
}
