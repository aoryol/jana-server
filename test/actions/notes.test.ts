import { Note } from "~/models/db";
import { WebContext } from "~/models/web";

import * as routes from "~/actions/notes";

import utils from "../test-utils";

describe("routes/notes", () => {
  utils.db.use();

  const getNote = async (id: string): Promise<Note> => utils.db.get("notes", id);
  const updateNote = async (id: string, fields: Partial<Note>): Promise<void> => utils.db.update("notes", id, fields);

  beforeEach(async () => {
    const note = (fields: Partial<Note>) => ({
      userId: utils.constants.DEFAULT_USER_OBJECT_ID,
      dateCreated: new Date("2018-01-01"),
      dateModified: new Date("2018-02-01"),
      title: "note title",
      body: "note body",
      ...fields
    });

    await utils.db.fill<Note>("notes", [
      note({ title: "note #1", _id: utils.constants.OBJECT_ID_1, dateModified: new Date("2018-03-01") }),
      note({ title: "note #2", isDeleted: false }),
      note({ title: "note #3", _id: utils.constants.OBJECT_ID_2, dateModified: new Date("2018-01-01") }),
      note({ title: "note #4", _id: utils.constants.OBJECT_ID_A, userId: utils.constants.OBJECT_ID_A }),
      note({ title: "note #5", isDeleted: true, dateModified: new Date("2018-03-20") })
    ]);
  });

  describe("#getList", () => {
    test("It returns non deleted user notes", async () => {
      const ctx = await utils.koa.runApiRoute(routes.getList);
      const notes = ctx.body;

      expect(notes).toBeInstanceOf(Array);
      expect(notes.length).toBe(3);

      expect(notes[0].isDeleted).toBe(false);
      expect(notes[1].isDeleted).toBe(false);

      expect(notes[0].id).toBe(utils.constants.OBJECT_ID_2.toHexString());

      expect(notes[0].body).toBeDefined();
    });

    test("It's ordered by date modified", async () => {
      const ctx = await utils.koa.runApiRoute(routes.getList);

      let prevDateModified = null;
      for (const dateModified of ctx.body.map((n: Note) => n.dateModified)) {
        expect(dateModified).toBeInstanceOf(Date);

        if (prevDateModified) {
          expect(dateModified.getTime()).toBeGreaterThanOrEqual(prevDateModified);
        }

        prevDateModified = dateModified.getTime();
      }
    });

    test("It allows to get only new notes", async () => {
      const ctx = await utils.koa.runApiRoute(routes.getList, {
        query: `timestamp=${new Date("2018-02-28").getTime()}`
      });

      expect(ctx.body.length).toBe(2);
      expect(ctx.body[1].isDeleted).toBe(true);
    });
  });

  describe("#getById", () => {
    test("It doesn't allow to get notes of other user", async () => {
      const ctxPromise = utils.koa.runApiRoute(routes.getById, {
        params: { id: utils.constants.OBJECT_ID_A.toHexString() }
      });

      await expect(ctxPromise).rejects.toMatchObject({ status: 404 });
    });

    test("It returns body of note", async () => {
      const ctx = await utils.koa.runApiRoute(routes.getById, {
        params: { id: utils.constants.OBJECT_ID_1.toHexString() }
      });

      expect(ctx.body.id).toBe(utils.constants.OBJECT_ID_1.toHexString());
      expect(ctx.body.body).toBe("note body");
    });
  });

  describe("#getByPublicName", () => {
    test("It allows to get other user published note", async () => {
      const id = utils.constants.OBJECT_ID_A.toHexString();
      const publicName = "some-public-name";

      await updateNote(id, { publicName });

      const ctx = await utils.koa.runApiRoute(routes.getByPublicName, { params: { publicName } });
      expect(ctx.body.id).toBe(id);
      expect(ctx.body.publicName).toBe(publicName);
    });
  });

  describe("#modify", () => {
    test("It allows to add note", async () => {
      const now1 = Date.now();

      const ctx = await utils.koa.runApiRoute(routes.modify, {
        query: `timestamp=${now1}`,
        body: [{ action: "add", title: "new note", body: "new note body" }]
      });

      const now2 = Date.now();
      const note = await getNote(ctx.body[0].id);

      expect(note.title).toBe("new note");
      expect(note.body).toBe("new note body");

      expect(note.dateCreated).toEqual(note.dateModified);
      expect(note.dateCreated.getTime()).toBeGreaterThanOrEqual(now1);
      expect(note.dateCreated.getTime()).toBeLessThanOrEqual(now2);
    });

    test("It allows to update note", async () => {
      const id = utils.constants.OBJECT_ID_1.toHexString();

      const ctx = await utils.koa.runApiRoute(routes.modify, {
        query: `timestamp=${Date.now()}`,
        body: [{ id, action: "update", title: "new note", body: "new note body" }]
      });

      const note = await getNote(id);

      expect(note.title).toBe("new note");
      expect(note.body).toBe("new note body");

      expect(note.dateModified.getTime()).toBeGreaterThan(new Date("2018-03-01").getTime());
    });

    test("It allows to delete note", async () => {
      const id = utils.constants.OBJECT_ID_1.toHexString();
      const ctx = await utils.koa.runApiRoute(routes.modify, {
        query: `timestamp=${Date.now()}`,
        body: [{ id, action: "remove" }]
      });
      const note = await getNote(id);

      expect(note.isDeleted).toBe(true);
    });

    test("It doesn't allow to delete other user note", async () => {
      const id = utils.constants.OBJECT_ID_A.toHexString();
      const ctx = await utils.koa.runApiRoute(routes.modify, {
        query: `timestamp=${Date.now()}`,
        body: [{ id, action: "remove" }]
      });
      const note = await getNote(id);

      expect(note.isDeleted).not.toBe(true);
    });

    test("It allows to create and remove note versions", async () => {
      const id = utils.constants.OBJECT_ID_1.toHexString();

      await utils.koa.runApiRoute(routes.modify, {
        query: `timestamp=${new Date("2018-02-01").getTime()}`,
        body: [{ id, action: "update", title: "new note", body: "new note body" }]
      });

      let oldNote = await getNote(id);
      expect(oldNote.title).toBe("note #1");

      let newNote = await getNote(oldNote.versions![0].toHexString());
      expect(newNote.title).toBe("new note");

      const newNoteId = newNote._id.toHexString();

      await utils.koa.runApiRoute(routes.modify, {
        query: `timestamp=${Date.now()}`,
        body: [{ id: newNoteId, action: "remove" }]
      });

      oldNote = await getNote(id);
      newNote = await getNote(newNoteId);

      expect(oldNote.versions!.length).toBe(0);
      expect(newNote.isDeleted).toBe(true);
    });
  });

  describe("#publish", () => {
    test("It generates good public name on publish", async () => {
      const id = utils.constants.OBJECT_ID_1.toHexString();
      const ctx = await utils.koa.runApiRoute(routes.publish, { params: { id } });

      const expectedPublicName = `note-1-${utils.constants.OBJECT_ID_1.toHexString()}`;
      expect(ctx.body.publicName).toBe(expectedPublicName);

      const note = await getNote(id);
      expect(note.publicName).toBe(expectedPublicName);
    });

    test("It doesn't allow to publish other user note", async () => {
      const ctxPromise = utils.koa.runApiRoute(routes.publish, {
        params: { id: utils.constants.OBJECT_ID_A.toHexString() }
      });

      await expect(ctxPromise).rejects.toMatchObject({ status: 404 });
    });
  });

  describe("#unpublish", () => {
    test("It removes public name", async () => {
      const id = utils.constants.OBJECT_ID_1.toHexString();
      await updateNote(id, { publicName: "some-public-name" });

      const noteBefore = await getNote(id);
      expect(noteBefore.publicName).toBe("some-public-name");

      await utils.koa.runApiRoute(routes.unpublish, { params: { id } });
      const noteAfter = await getNote(id);
      expect(noteAfter.publicName).toBeUndefined();
    });

    test("It doesn't allow to unpublish other user note", async () => {
      const ctxPromise = utils.koa.runApiRoute(routes.unpublish, {
        params: { id: utils.constants.OBJECT_ID_A.toHexString() }
      });

      await expect(ctxPromise).rejects.toMatchObject({ status: 404 });
    });
  });
});
