import * as utils from "~/actions/utils";

describe("#parseBoolean", () => {
  test("It returns 'true' for truthy values", () => {
    expect(utils.parseBoolean("1")).toBe(true);
    expect(utils.parseBoolean("true")).toBe(true);
    expect(utils.parseBoolean("True")).toBe(true);
  });

  test("It returns 'false' for falsy values", () => {
    expect(utils.parseBoolean("0")).toBe(false);
    expect(utils.parseBoolean("false")).toBe(false);
    expect(utils.parseBoolean("False")).toBe(false);
    expect(utils.parseBoolean(undefined)).toBe(false);
    expect(utils.parseBoolean("")).toBe(false);
  });
});
