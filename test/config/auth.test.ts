import { decode as decodeJwt } from "jsonwebtoken";
import { parse as parseQueryString } from "querystring";
import { parse as parseUrl } from "url";

import auth from "~/config/auth";

import * as userActions from "~/actions/users";
import { UserClient } from "~/models/db";
import { WebContext } from "~/models/web";

import utils from "../test-utils";

describe("router", () => {
  const runRoute = (url: string, ctxProps?: any): Promise<WebContext> =>
    utils.koa.runMiddleware(auth.router.routes(), `http://example.com:321/${url}`, ctxProps);

  const redirectUrlParam = (redirectUrl: string) => `redirectUrl=${encodeURIComponent(redirectUrl)}`;

  utils.db.use();

  const allowedRedirectUrl = "http://example.com:123";
  const allowedRedirectUrlParam = redirectUrlParam(allowedRedirectUrl);

  utils.settings.mock("auth.allowedRedirectUrls", [allowedRedirectUrl]);
  utils.settings.mock("server.web.url", "http://example.com:321");

  describe("/github", () => {
    const forbiddenRedirectUrlParam = redirectUrlParam("http://forbidden-url");

    test("It disallows forbidden URLs", async () => {
      const route = runRoute(`github?${forbiddenRedirectUrlParam}`);
      await expect(route).rejects.toMatchObject({ status: 403 });
    });

    test("It redirects to github with valid parameters", async () => {
      const ctx = await runRoute(`github?${allowedRedirectUrlParam}`);

      const redirectLocation = ctx.res.getHeader("location");
      expect(redirectLocation).toBeDefined();

      const parsedRedirectUrl = parseUrl(redirectLocation as string);
      expect(parsedRedirectUrl.host).toBe("github.com");

      const queryString = parseQueryString(parsedRedirectUrl.query || "");
      expect(queryString.client_id).toBe("__test");

      expect(queryString.redirect_uri).toBe(`http://example.com:321/auth/github/callback?${allowedRedirectUrlParam}`);
    });
  });

  describe("/github/callback", () => {
    let mockFns: jest.SpyInstance[];

    beforeAll(async () => {
      const githubOAuth = (auth.passport as any)._strategy("github")._oauth2;

      const getTokenFn = jest.spyOn(githubOAuth, "getOAuthAccessToken");
      getTokenFn.mockImplementation(
        (code: string, params: any, cb: (err: any, accessToken: string) => void) => {
          cb(null, "Access Token");
        }
      );

      const getProfileFn = jest.spyOn(githubOAuth, "get");
      getProfileFn.mockImplementation(
        (profileUrl: string, accessToken: string, cb: (err: any, body: string) => void) => {
          cb(null, JSON.stringify({ externalId: "user_id", displayName: "user_name" }));
        }
      );

      mockFns = [getTokenFn, getProfileFn];
    });

    afterAll(() => {
      for (const mockFn of mockFns) {
        mockFn.mockRestore();
      }
    });

    test("It redirects to provided URL with JWT token", async () => {
      const ctx = await runRoute(`github/callback?code=auth_code&${allowedRedirectUrlParam}`);

      const redirectLocation = ctx.res.getHeader("location");
      expect(redirectLocation).toBeDefined();

      const urlParts = /^(.*)#token=(.*)$/.exec(redirectLocation as string);
      expect(urlParts![1]).toEqual(allowedRedirectUrl);

      const decodedJwt: any = decodeJwt(urlParts![2]);
      expect(decodedJwt.id).toBeDefined();
    });
  });

  describe("/refresh", () => {
    let updateClientsFn: jest.SpyInstance;

    beforeAll(() => {
      updateClientsFn = jest.spyOn(userActions, "updateClients");
      updateClientsFn.mockImplementation(() => undefined);
    });

    afterAll(() => {
      updateClientsFn.mockRestore();
    });

    test("It allows to set client on token refresh", async () => {
      const token = auth.makeToken({ id: "user id", displayName: "user" });
      const headers = { authorization: `Bearer ${token}` };
      const body = { client: { id: 123, name: "client" } };
      const ctx = await runRoute("refresh", { method: "POST", headers, body });

      const decodedJwt: any = decodeJwt(ctx.body.token);
      expect(decodedJwt.id).toBe("user id");
      expect(decodedJwt.client.id).toBe(123);

      const dbUserClient: UserClient = updateClientsFn.mock.calls[0][1];
      expect(dbUserClient.id).toBe(123);
    });
  });
});
