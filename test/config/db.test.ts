import * as _ from "lodash";
import mongo from "mongodb";

import * as db from "~/config/db";
import * as models from "~/models/db";

describe("#connect", () => {
  let instance: models.Database;

  beforeAll(async () => {
    instance = await db.connect();
  });

  afterAll(async () => {
    db.disconnect();
  });

  test("It creates all required collections", async () => {
    const collections = await instance.listCollections().toArray();
    const collectionNames = _.map(collections, "name");

    expect(collectionNames).toEqual(expect.arrayContaining([
      "users",
      "notes"
    ]));
  });

  test("It creates only one connection", async () => {
    await expect(db.connect()).rejects.toBeInstanceOf(Error);
  });

  test("It exports database as module variable", () => {
    expect(db.database).toBe(instance);
  });

  describe("Utility methods", () => {
    const hexId = "123456789012345678901234";

    test("#objectId", () => {
      const id = instance.objectId(hexId);
      expect(id).toBeInstanceOf(mongo.ObjectID);
      expect(id.toHexString()).toBe(hexId);
    });

    test("#stringifyEntityId", () => {
      const otherFields = {
        field1: 1,
        field2: "text"
      };

      const sourceEntity = {
        _id: new mongo.ObjectID(hexId),
        ...otherFields
      };

      const resultEntity = instance.stringifyEntityId(sourceEntity);

      expect(resultEntity).toEqual({ id: hexId, ...otherFields });
    });
  });
});

describe("#disconnect", () => {
  test("Can be called if already disconnected", async () => {
    await expect(db.disconnect()).resolves.not.toBeInstanceOf(Error);
  });

  test("Can be called if connection exists", async () => {
    await db.connect();
    await expect(db.disconnect()).resolves.not.toBeInstanceOf(Error);
  });

  test("Allows reconnect", async () => {
    await db.connect();
    await db.disconnect();

    await expect(db.connect()).resolves.not.toBeInstanceOf(Error);
    await db.disconnect();
  });
});
